import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import supabase from 'vue-3-supabase'

// createApp(App).mount('#app')


// Import supabase

 const app = createApp(App)
 app.component('test', {
    // ... options ...
  })
  app.component('spot', {
     // ... options ...
   })

// Use supabase
app.use(supabase, {
  supabaseUrl: 'https://fxcebsajvdlnwpmmodab.supabase.co', // actually you can use something like import.meta.env.VITE_SUPABASE_URL
  supabaseKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTY0MTQ1NzczMSwiZXhwIjoxOTU3MDMzNzMxfQ.gt2mp51qKMtSPdwjIEQGionSAPecpBcov2EyEiaa9EA', // actually you can use something like import.meta.env.VITE_SUPABASE_KEY,
  options: {}
})

app.mount('#app')

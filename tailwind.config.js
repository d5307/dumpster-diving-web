module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}", 
    "./src/App.vue",  
    "./src/components/spot.vue",  
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
